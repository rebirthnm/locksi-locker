import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/not-available',
    name: 'Busy',
    component: () => import('../views/NotAvailable')
  },
  {
    path: '/invalid-qr',
    name: 'InvalidQr',
    component: () => import('../views/InvalidQr')
  },
  {
    path: '/insufficient',
    name: 'Insufficient',
    component: () => import('../views/Insufficient')
  },
  {
    path: '/topup-success',
    name: 'TopupSuccess',
    component: () => import('../views/TopupSuccess')
  },
  {
    path: '/summary',
    name: 'Summary',
    component: () => import('../views/Summary')
  },
  {
    path: '/loading',
    name: 'Loading',
    component: () => import('../views/Loading')
  },
  {
    path: '/getback',
    name: 'Getback',
    component: () => import('../views/GetbackSummary')
  },
  {
    path: '/getback-overtime',
    name: 'GetbackOvertime',
    component: () => import('../views/GetbackSummary')
  },
  {
    path: '*',
    name: '404',
    component: () => import('../views/PageNotFound')
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
